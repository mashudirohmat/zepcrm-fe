// import menuPages from './menus/pages.menu'

export default {
  // main navigation - side menu
  menu: [{
    text: '',
    key: '',
    items: [
      { icon: 'mdi-alpha-c-box-outline', key: "menu.crm", text: 'CRM', link: '/partner' },
      { icon: 'mdi-pencil-outline', key: "menu.settings", text: 'Master', link: '/blank' }
    ]
  },
  //  {
  //   text: 'Pages',
  //   key: 'menu.pages',
  //   items: menuPages
  // }
],

  // footer links
  // footer: [{
  //   text: 'Docs',
  //   key: 'menu.docs',
  //   href: 'https://vuetifyjs.com',
  //   target: '_blank'
  // }]
}
